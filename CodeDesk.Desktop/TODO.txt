﻿在C#中，使用P/Invoke调用Linux的X11库可以实现创建多个窗体，包含无边框和有边框，无边框支持拖动、放大缩小和关闭功能，并为每个窗体创建自己的消息循环。下面是一个简单的例子，演示如何实现这些功能：

首先，需要导入X11库的头文件，以及定义所需的函数和结构体。在C#中，可以使用 DllImport 特性来导入外部库。以下是所需的导入声明和结构体定义：

using System;
using System.Runtime.InteropServices;

public class X11
{
    // 导入X11库
    [DllImport("libX11", EntryPoint = "XOpenDisplay")]
    public static extern IntPtr XOpenDisplay(IntPtr display);

    [DllImport("libX11", EntryPoint = "XCreateWindow")]
    public static extern IntPtr XCreateWindow(IntPtr display, IntPtr parent, int x, int y, int width, int height, int border_width, int depth, int window_class, IntPtr visual, IntPtr value_mask, ref XSetWindowAttributes attributes);

    [DllImport("libX11", EntryPoint = "XDestroyWindow")]
    public static extern int XDestroyWindow(IntPtr display, IntPtr window);

    [DllImport("libX11", EntryPoint = "XMapWindow")]
    public static extern int XMapWindow(IntPtr display, IntPtr window);

    [DllImport("libX11", EntryPoint = "XUnmapWindow")]
    public static extern int XUnmapWindow(IntPtr display, IntPtr window);

    [DllImport("libX11", EntryPoint = "XMoveWindow")]
    public static extern int XMoveWindow(IntPtr display, IntPtr window, int x, int y);

    [DllImport("libX11", EntryPoint = "XResizeWindow")]
    public static extern int XResizeWindow(IntPtr display, IntPtr window, int width, int height);

    [DllImport("libX11", EntryPoint = "XSetWMProtocols")]
    public static extern int XSetWMProtocols(IntPtr display, IntPtr window, IntPtr protocols, int count);

    [DllImport("libX11", EntryPoint = "XNextEvent")]
    public static extern int XNextEvent(IntPtr display, ref XEvent xevent);

    [DllImport("libX11", EntryPoint = "XSendEvent")]
    public static extern int XSendEvent(IntPtr display, IntPtr window, bool propagate, IntPtr event_mask, ref XEvent xevent);

    [DllImport("libX11", EntryPoint = "XFlush")]
    public static extern int XFlush(IntPtr display);

    // 定义所需的结构体和常量
    public const int ExposureMask = 1;
    public const int ButtonPressMask = 256;
    public const int ButtonReleaseMask = 512;
    public const int Button1MotionMask = 8192;
    public const int Button2MotionMask = 16384;
    public const int Button3MotionMask = 32768;
    public const int Button4MotionMask = 65536;
    public const int Button5MotionMask = 131072;
    public const int KeyPressMask = 1;
    public const int KeyReleaseMask = 2;
    public const int PointerMotionMask = 64;

    public const int ButtonPress = 4;
    public const int ButtonRelease = 5;
    public const int MotionNotify = 6;
    public const int KeyPress = 2;
    public const int KeyRelease = 3;

    public const int CWEventMask = (1 << 11);

    [StructLayout(LayoutKind.Sequential)]
    public struct XEvent
    {
        public int type;
        public IntPtr xany;
        public IntPtr xkey;
        public IntPtr xbutton;
        public IntPtr xmotion;
        public IntPtr xcrossing;
        public IntPtr xfocus;
        public IntPtr xexpose;
        public IntPtr xgraphicsexpose;
        public IntPtr xnoexpose;
        public IntPtr xvisibility;
        public IntPtr xcreatewindow;
        public IntPtr xdestroywindow;
        public IntPtr xunmap;
        public IntPtr xmap;
        public IntPtr xmaprequest;
        public IntPtr xreparent;
        public IntPtr xconfigure;
        public IntPtr xgravity;
        public IntPtr xresizerequest;
        public IntPtr xconfigurerequest;
        public IntPtr xcirculate;
        public IntPtr xcirculaterequest;
        public IntPtr xproperty;
        public IntPtr xselectionclear;
        public IntPtr xselectionrequest;
        public IntPtr xselection;
        public IntPtr xcolormap;
        public IntPtr xclient;
        public IntPtr xmapping;
        public IntPtr xerror;
        public IntPtr xkeymap;
        public IntPtr xgeneric;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct XSetWindowAttributes
    {
        public IntPtr background_pixmap;
        public IntPtr background_pixel;
        public IntPtr border_pixmap;
        public IntPtr border_pixel;
        public int bit_gravity;
        public int win_gravity;
        public int backing_store;
        public IntPtr backing_planes;
        public IntPtr backing_pixel;
        public bool save_under;
        public IntPtr event_mask;
        public IntPtr do_not_propagate_mask;
        public bool override_redirect;
        public IntPtr colormap;
        public IntPtr cursor;
    }
}

然后，可以创建一个窗体类，该类封装了创建、显示、移动、调整大小和销毁窗体的方法，以及处理窗体事件的消息循环。以下是一个简单的窗体类的示例：

using System;

public class Window
{
    private IntPtr display;
    private IntPtr window;
    private bool isBorderless;
    private int x, y, width, height;

    public Window(bool borderless, int x, int y, int width, int height)
    {
        display = X11.XOpenDisplay(IntPtr.Zero);
        isBorderless = borderless;
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }

    public void Create()
    {
        var screen = X11.XDefaultScreen(display);
        var root = X11.XRootWindow(display, screen);

        var windowAttributes = new X11.XSetWindowAttributes();
        windowAttributes.event_mask = new IntPtr(X11.ExposureMask | X11.ButtonPressMask | X11.ButtonReleaseMask | X11.Button1MotionMask | X11.Button2MotionMask | X11.Button3MotionMask | X11.Button4MotionMask | X11.Button5MotionMask | X11.KeyPressMask | X11.KeyReleaseMask | X11.PointerMotionMask);

        if (isBorderless)
        {
            windowAttributes.override_redirect = true;
        }

        window = X11.XCreateWindow(display, root, x, y, width, height, 0, 0, 0, IntPtr.Zero, (IntPtr)X11.CWEventMask, ref windowAttributes);

        if (!isBorderless)
        {
            var protocols = X11.XInternAtom(display, "WM_PROTOCOLS", false);
            var deleteWindow = X11.XInternAtom(display, "WM_DELETE_WINDOW", false);
            X11.XSetWMProtocols(display, window, ref protocols, 1);
            X11.XSetWMProtocols(display, window, ref deleteWindow, 1);
        }

        X11.XMapWindow(display, window);
        X11.XFlush(display);
    }

    public void Show()
    {
        X11.XMapWindow(display, window);
        X11.XFlush(display);
    }

    public void Move(int x, int y)
    {
        this.x = x;
        this.y = y;
        X11.XMoveWindow(display, window, x, y);
        X11.XFlush(display);
    }

    public void Resize(int width, int height)
    {
        this.width = width;
        this.height = height;
        X11.XResizeWindow(display, window, width, height);
        X11.XFlush(display);
    }

    public void Destroy()
    {
        X11.XDestroyWindow(display, window);
        X11.XFlush(display);
    }

    public void RunMessageLoop()
    {
        var xevent = new X11.XEvent();
        while (true)
        {
            X11.XNextEvent(display, ref xevent);

            switch (xevent.type)
            {
                case X11.Expose:
                    // 处理窗体重绘事件
                    break;
                case X11.ButtonPress:
                    // 处理鼠标按下事件
                    break;
                case X11.ButtonRelease:
                    // 处理鼠标释放事件
                    break;
                case X11.MotionNotify:
                    // 处理鼠标移动事件
                    break;
                case X11.KeyPress:
                    // 处理键盘按下事件
                    break;
                case X11.KeyRelease:
                    // 处理键盘释放事件
                    break;
                default:
                    break;
            }
        }
    }
}

最后，可以使用窗体类创建多个窗体，并分别运行它们的消息循环。以下是一个简单的示例：

public class Program
{
    public static void Main(string[] args)
    {
        // 创建有边框的窗体
        var borderedWindow = new Window(false, 100, 100, 400, 300);
        borderedWindow.Create();

        // 创建无边框的窗体
        var borderlessWindow = new Window(true, 200, 200, 300, 200);
        borderlessWindow.Create();

        // 运行窗体的消息循环
        borderedWindow.RunMessageLoop();
        borderlessWindow.RunMessageLoop();
    }
}

public void SetIcon(string iconFilePath)
{
    // 读取图标文件
    using (var fileStream = new FileStream(iconFilePath, FileMode.Open, FileAccess.Read))
    {
        using (var binaryReader = new BinaryReader(fileStream))
        {
            var fileBytes = binaryReader.ReadBytes((int)fileStream.Length);

            // 创建XImage结构体
            var ximage = new XImage();
            ximage.format = ZPixmap;
            ximage.data = Marshal.AllocHGlobal(fileBytes.Length);
            ximage.width = 32; // 图标宽度
            ximage.height = 32; // 图标高度
            ximage.bitmap_unit = 32;
            ximage.bitmap_bit_order = LSBFirst;
            ximage.bitmap_pad = 32;
            ximage.bytes_per_line = 0;
            ximage.bits_per_pixel = 32;
            ximage.red_mask = 0xFF0000;
            ximage.green_mask = 0x00FF00;
            ximage.blue_mask = 0x0000FF;
            ximage.obdata = IntPtr.Zero;

            // 将图标数据复制到XImage
            Marshal.Copy(fileBytes, 0, ximage.data, fileBytes.Length);

            // 创建XWMHints结构体
            var wmHints = new XWMHints();
            wmHints.flags = IconPixmapHint;
            wmHints.icon_pixmap = XCreatePixmap(display, window, ximage.width, ximage.height, 32);

            // 将XImage绘制到XPMap
            XPutImage(display, wmHints.icon_pixmap, DefaultGC(display, 0), ref ximage, 0, 0, 0, 0, ximage.width, ximage.height);

            // 设置窗体的图标
            XSetWMHints(display, window, ref wmHints);

            // 释放资源
            XDestroyImage(ref ximage);
            XFreePixmap(display, wmHints.icon_pixmap);
        }
    }
}

public class Program
{
    public static void Main(string[] args)
    {
        // 创建有边框的窗体
        var borderedWindow = new Window(false, 100, 100, 400, 300);
        borderedWindow.Create();
        borderedWindow.SetIcon("path/to/icon.png");

        // 创建无边框的窗体
        var borderlessWindow = new Window(true, 200, 200, 300, 200);
        borderlessWindow.Create();
        borderlessWindow.SetIcon("path/to/icon.png");

        // 创建并启动两个线程，分别运行两个窗体的消息循环
        var borderedThread = new Thread(borderedWindow.RunMessageLoop);
        borderedThread.Start();

        var borderlessThread = new Thread(borderlessWindow.RunMessageLoop);
        borderlessThread.Start();

        // 等待两个线程结束
        borderedThread.Join();
        borderlessThread.Join();
    }



    要判断是否存在X11环境，可以使用 XOpenDisplay 函数来尝试打开X11显示。如果成功打开，表示存在X11环境，否则表示不存在。以下是一个示例代码：

using System;
using System.Runtime.InteropServices;

public class X11
{
    [DllImport("libX11", EntryPoint = "XOpenDisplay")]
    public static extern IntPtr XOpenDisplay(IntPtr display);

    [DllImport("libX11", EntryPoint = "XCloseDisplay")]
    public static extern void XCloseDisplay(IntPtr display);
}

public class Program
{
    public static void Main(string[] args)
    {
        IntPtr display = X11.XOpenDisplay(IntPtr.Zero);
        if (display != IntPtr.Zero)
        {
            Console.WriteLine("X11 environment is available.");
            X11.XCloseDisplay(display);
        }
        else
        {
            Console.WriteLine("X11 environment is not available.");
        }
    }
}




要在X11创建的窗体中使用WebKitGTK+，您需要进行以下步骤：

1. 安装WebKitGTK+库：首先，您需要在您的Linux系统上安装WebKitGTK+库。可以使用包管理器来安装，例如在Ubuntu上可以使用以下命令：
sudo apt-get install libwebkit2gtk-4.0-dev

2. 创建X11窗体：使用X11相关的库和函数，您可以创建一个X11窗体。这可能涉及到使用Xlib或其他库来创建窗口、处理事件等。

3. 初始化WebKitGTK+：在您的C#代码中，您需要初始化WebKitGTK+库。您可以使用P/Invoke来调用WebKitGTK+的函数。以下是一个简单的示例代码：
[DllImport("libwebkit2gtk-4.0.so")]
private static extern void webkit_web_view_init(IntPtr webView);

[DllImport("libwebkit2gtk-4.0.so")]
private static extern IntPtr webkit_web_view_new();

// 在窗体创建后调用此方法来初始化WebKitGTK+
private void InitializeWebKitGTK()
{
    webkit_web_view_init(IntPtr.Zero);
}

4. 将WebKitGTK+视图嵌入到窗体中：使用X11的函数，您可以将WebKitGTK+视图嵌入到您创建的X11窗体中。以下是一个简单的示例代码：
[DllImport("libgdk-3.so")]
private static extern IntPtr gdk_x11_window_get_xid(IntPtr window);

[DllImport("libwebkit2gtk-4.0.so")]
private static extern void webkit_web_view_set_window(IntPtr webView, IntPtr window);

// 将WebKitGTK+视图嵌入到X11窗体中
private void EmbedWebKitGTKView(IntPtr window)
{
    IntPtr webView = webkit_web_view_new();
    webkit_web_view_set_window(webView, gdk_x11_window_get_xid(window));
}




要在X11创建的窗体中使用GeckoFX，您需要进行以下步骤：

1. 安装GeckoFX库：首先，您需要在您的C#项目中安装GeckoFX库。您可以使用NuGet包管理器来安装GeckoFX库，或者从官方网站下载并手动安装。

2. 创建X11窗体：使用X11相关的库和函数，您可以创建一个X11窗体。这可能涉及到使用Xlib或其他库来创建窗口、处理事件等。

3. 初始化GeckoFX：在您的C#代码中，您需要初始化GeckoFX库。您可以使用GeckoFX的类和方法来实现。以下是一个简单的示例代码：
// 在窗体创建后调用此方法来初始化GeckoFX
private void InitializeGeckoFX()
{
    Gecko.Xpcom.Initialize("libxul.so");
}

4. 将GeckoFX视图嵌入到窗体中：使用X11的函数，您可以将GeckoFX视图嵌入到您创建的X11窗体中。以下是一个简单的示例代码：
// 将GeckoFX视图嵌入到X11窗体中
private void EmbedGeckoFXView(IntPtr window)
{
    GeckoWebBrowser browser = new GeckoWebBrowser();
    browser.CreateControl();
    browser.Parent = this;
    browser.Dock = DockStyle.Fill;
    browser.Handle = window;
}


以下是在Linux的X11窗体中使用CefGlue的基本步骤：

1. 安装CefGlue库：首先，您需要在您的C#项目中安装CefGlue库。您可以从CefGlue的官方网站下载并手动安装。

2. 创建X11窗体：使用X11相关的库和函数，您可以创建一个X11窗体。这可能涉及到使用Xlib或其他库来创建窗口、处理事件等。

3. 初始化CefGlue：在您的C#代码中，您需要初始化CefGlue库。以下是一个简单的示例代码：
// 在窗体创建后调用此方法来初始化CefGlue
private void InitializeCefGlue()
{
    CefRuntime.Load();
    CefSettings settings = new CefSettings();
    CefRuntime.Initialize(settings);
}

4. 将CefGlue视图嵌入到窗体中：使用X11的函数，您可以将CefGlue视图嵌入到您创建的X11窗体中。以下是一个简单的示例代码：
using Xilium.CefGlue;

// 将CefGlue视图嵌入到X11窗体中
private void EmbedCefGlueView(IntPtr window)
{
    CefWindowInfo windowInfo = CefWindowInfo.Create();
    windowInfo.SetAsChild(window, 0, 0, Width, Height);
    
    CefBrowserSettings browserSettings = new CefBrowserSettings();
    
    CefBrowserHost.CreateBrowser(windowInfo, new CefClient(), "about:blank", browserSettings);
}